<?php
/**
 * @file
 * accounting_content_types.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function accounting_content_types_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_locked'.
  $permissions['create field_locked'] = array(
    'name' => 'create field_locked',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_part_from_reference'.
  $permissions['create field_part_from_reference'] = array(
    'name' => 'create field_part_from_reference',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_part_to_reference'.
  $permissions['create field_part_to_reference'] = array(
    'name' => 'create field_part_to_reference',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_system_journal'.
  $permissions['create field_system_journal'] = array(
    'name' => 'create field_system_journal',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_trail'.
  $permissions['create field_trail'] = array(
    'name' => 'create field_trail',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_locked'.
  $permissions['edit field_locked'] = array(
    'name' => 'edit field_locked',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_part_from_reference'.
  $permissions['edit field_part_from_reference'] = array(
    'name' => 'edit field_part_from_reference',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_part_to_reference'.
  $permissions['edit field_part_to_reference'] = array(
    'name' => 'edit field_part_to_reference',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_system_journal'.
  $permissions['edit field_system_journal'] = array(
    'name' => 'edit field_system_journal',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_trail'.
  $permissions['edit field_trail'] = array(
    'name' => 'edit field_trail',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_locked'.
  $permissions['edit own field_locked'] = array(
    'name' => 'edit own field_locked',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_part_from_reference'.
  $permissions['edit own field_part_from_reference'] = array(
    'name' => 'edit own field_part_from_reference',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_part_to_reference'.
  $permissions['edit own field_part_to_reference'] = array(
    'name' => 'edit own field_part_to_reference',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_system_journal'.
  $permissions['edit own field_system_journal'] = array(
    'name' => 'edit own field_system_journal',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_trail'.
  $permissions['edit own field_trail'] = array(
    'name' => 'edit own field_trail',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_locked'.
  $permissions['view field_locked'] = array(
    'name' => 'view field_locked',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_part_from_reference'.
  $permissions['view field_part_from_reference'] = array(
    'name' => 'view field_part_from_reference',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_part_to_reference'.
  $permissions['view field_part_to_reference'] = array(
    'name' => 'view field_part_to_reference',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_system_journal'.
  $permissions['view field_system_journal'] = array(
    'name' => 'view field_system_journal',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_trail'.
  $permissions['view field_trail'] = array(
    'name' => 'view field_trail',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_locked'.
  $permissions['view own field_locked'] = array(
    'name' => 'view own field_locked',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_part_from_reference'.
  $permissions['view own field_part_from_reference'] = array(
    'name' => 'view own field_part_from_reference',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_part_to_reference'.
  $permissions['view own field_part_to_reference'] = array(
    'name' => 'view own field_part_to_reference',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_system_journal'.
  $permissions['view own field_system_journal'] = array(
    'name' => 'view own field_system_journal',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
      'customer' => 'customer',
      'timekeeper' => 'timekeeper',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_trail'.
  $permissions['view own field_trail'] = array(
    'name' => 'view own field_trail',
    'roles' => array(
      'accountant' => 'accountant',
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
