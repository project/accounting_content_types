<?php
/**
 * @file
 * accounting_content_types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function accounting_content_types_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_item_two|node|transaction|form';
  $field_group->group_name = 'group_item_two';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'transaction';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Transaction details',
    'weight' => '7',
    'children' => array(
      0 => 'field_part_from_reference',
      1 => 'field_part_to_reference',
      2 => 'field_project_reference',
      3 => 'field_attachment',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Transaction details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_item_two|node|transaction|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Transaction details');

  return $field_groups;
}
